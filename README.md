# Great Place Project

Little application to study of how interact with device in flutter, and how to use maps from OpenStreetMaps.

## Resources
- Image Picker Package: https://pub.dev/packages/image_picker
- Location Package: https://pub.dev/packages/location
- Path Provider Package: https://pub.dev/packages/path_provider
- Path Package: https://pub.dev/packages/path
- Flutter Map Package: https://pub.dev/packages/flutter_map
- SQFLite Package: https://pub.dev/packages/sqflite

## Notes

- [OpenStreetMap](https://www.openstreetmap.org)
- [Project from Udemy](https://www.udemy.com/share/1013o4A0QbdFtRR3o=/)
- View Flutter [online documentation](https://flutter.dev/docs), which offers tutorials, samples, guidance on mobile development, and a full API reference.
