import 'package:latlong/latlong.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import '../secret_keys.dart';

class LocationHelper {
  static String generateLocationPreviewImage(LatLng position) {
    return _generateMapboxPreviewImage(position);
  }

  static const MAPBOX_URL_TEMPLATE =
      'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}@2x.png?access_token={accessToken}';
  static const MAPBOX_ACCESS_TOKEN = MAPBOX_TOKEN;

  static String _generateMapboxPreviewImage(LatLng position) {
    return 'https://api.mapbox.com/styles/v1/mapbox/streets-v11/static'
        '/pin-s-a+00FF00(${position.longitude},${position.latitude})'
        '/${position.longitude},${position.latitude},16'
        '/600x300@2x'
        '?access_token=$MAPBOX_ACCESS_TOKEN';
  }

  static Future<String> getPlaceAddress(LatLng position) async {
    var url =
        'https://api.mapbox.com/geocoding/v5/mapbox.places/${position.longitude},${position.latitude}.json?access_token=$MAPBOX_ACCESS_TOKEN&autocomplete=false';
    var response = await http.get(url);
    return json.decode(response.body)['features'][0]['place_name'];
  }
}
