import 'package:latlong/latlong.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';

import '../helpers/location_helper.dart';
import '../models/place.dart';

class MapScreen extends StatefulWidget {
  final PlaceLocation initialLocation;
  final bool isSelecting;

  const MapScreen(
      {this.initialLocation =
          const PlaceLocation(latitude: 41.979735, longitude: 12.047528),
      this.isSelecting = false});

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  LatLng _pickedLocations;

  void _selectLocation(LatLng position) {
    setState(() {
      _pickedLocations = position;
    });
  }

  @override
  Widget build(BuildContext context) {
    var marker = Marker(
      width: 80.0,
      height: 80.0,
      point: _pickedLocations ?? LatLng(widget.initialLocation.latitude, widget.initialLocation.longitude),
      builder: (ctx) => Container(
        child: Icon(
          Icons.location_on,
          color: Colors.green,
          size: 40,
        ),
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text("Your Map"),
        actions: <Widget>[
          if (widget.isSelecting)
            IconButton(
              icon: Icon(Icons.check),
              onPressed: _pickedLocations == null
                  ? null
                  : () {
                      Navigator.of(context).pop(_pickedLocations);
                    },
            ),
        ],
      ),
      body: FlutterMap(
        options: MapOptions(
          onTap: widget.isSelecting ? _selectLocation : null,
          center: LatLng(widget.initialLocation.latitude,
              widget.initialLocation.longitude),
          zoom: 15.0,
        ),
        layers: [
          TileLayerOptions(
            urlTemplate: LocationHelper.MAPBOX_URL_TEMPLATE,
            additionalOptions: {
              'accessToken': LocationHelper.MAPBOX_ACCESS_TOKEN,
              'id': 'mapbox.streets',
            },
          ),
          MarkerLayerOptions(
            markers:
                _pickedLocations == null && widget.isSelecting ? [] : [marker],
          ),
        ],
      ),
    );
  }
}
