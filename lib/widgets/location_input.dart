import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import '../helpers/location_helper.dart';
import '../screens/map_screen.dart';
import 'package:latlong/latlong.dart';

class LocationInput extends StatefulWidget {
  final Function onSelectPlace;

  LocationInput(this.onSelectPlace);

  @override
  _LocationInputState createState() => _LocationInputState();
}

class _LocationInputState extends State<LocationInput> {
  String _previewImageUrl;

  Future<void> _getCurrentUserLocation() async {
    try {
      final locData = await Geolocator().getCurrentPosition();
      _getLocationPreview(locData);
      widget.onSelectPlace(LatLng(locData.latitude, locData.longitude));
    } catch (e) {
      return;
    }
  }

  void _getLocationPreview(Position locData) {
    final staticMapImageUrl = LocationHelper.generateLocationPreviewImage(
        LatLng(locData.latitude, locData.longitude));
    setState(() {
      _previewImageUrl = staticMapImageUrl;
    });

    widget.onSelectPlace(LatLng(locData.latitude, locData.longitude));
  }

  Future<void> _selectOnMap() async {
    final selectedLocation = await Navigator.of(context).push<LatLng>(
      MaterialPageRoute(
        fullscreenDialog: true,
        builder: (ctx) => MapScreen(
          isSelecting: true,
        ),
      ),
    );
    if (selectedLocation == null) {
      return;
    }
    _getLocationPreview(Position(
        latitude: selectedLocation.latitude,
        longitude: selectedLocation.longitude));
    widget.onSelectPlace(selectedLocation);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 170,
          width: double.infinity,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            border: Border.all(width: 1, color: Colors.grey),
          ),
          child: _previewImageUrl == null
              ? Text(
                  'No Location Choosen',
                  textAlign: TextAlign.center,
                )
              : Image.network(
                  _previewImageUrl,
                  fit: BoxFit.cover,
                  width: double.infinity,
                ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FlatButton.icon(
              icon: Icon(Icons.location_on),
              label: Text('Current Location'),
              textColor: Theme.of(context).primaryColor,
              onPressed: _getCurrentUserLocation,
            ),
            FlatButton.icon(
              icon: Icon(Icons.map),
              label: Text('Select on Map'),
              textColor: Theme.of(context).primaryColor,
              onPressed: _selectOnMap,
            ),
          ],
        ),
      ],
    );
  }
}
